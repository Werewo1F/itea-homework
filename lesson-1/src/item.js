import React, {Component} from 'react';

class Item extends Component {
  state = {
    arrived: false
  }

  changeStatus = () => {
    this.setState({arrived: !this.state.arrived})
  }

  render() {
    const {guest} = this.props;
    const {arrived} = this.state;
    const {changeStatus} = this;
    return (
      <div className={arrived ? "item arrived" : "item"}>
        <p className="info">
          Гость <b>{guest.name}</b> работает в компании <b>“{guest.company}”.</b> <br/>
          Его контакты: <br/>
          <b>{guest.phone};</b> <br/>
          <b>{guest.address}</b>
        </p>

        <button className="btn" onClick={changeStatus}>Прибыл</button>
      </div>
    );
  }
}

export default Item;