import React, {Component} from 'react';
import './App.css';

import data from './guests.json'
import logo from './logo.jpg'

import Item from './item.js';

class App extends Component {
  state = {
    guests: data
  }

  search = (e) => {
    let query = e.target.value.toLowerCase();
    let filteredData = data.filter(guest => {
      let name = guest.name.toLowerCase();
      if (name.indexOf(query) !== -1) {
        return guest;
      } else {
        return null;
      }
    });

    this.setState({guests: filteredData});
  }

  render() {
    const {guests} = this.state;
    const {search} = this;
    return (
      <div className="wrap">
        <header className="header">
          <div className="title">
            <span className="pretitle">Список жертв</span>
            <h1>Список гостей</h1>
          </div>
          <a href="/" className="logo">
            <img src={logo} alt="Logo"/>
          </a>
          <input type="text" onChange={search} placeholder="Введите имя гостя для поиска"/>
        </header>
        {
          (guests.length > 0) ? (
            guests.map(guest =>
              <Item
                key={guest.index}
                guest={guest}
              />
            )
          ) : (
            <div className="empty">
              empty
            </div>
          )
        }
      </div>
    );
  }
}

export default App;
