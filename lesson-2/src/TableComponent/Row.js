import React from 'react';

const Row = ({head, children}) => {
  return (
    (children) ? (
      <tr className={head ? 'is-head' : null}>
        {children}
      </tr>
    ) : (
      null
    )
  )
}

Row.defaultProps = {
  head: false
}

export default Row;
