import React from 'react';
import './style.css';

const Table = ({children}) => {
  return (
    (children) ? (
      <table>
        <tbody>
        {children}
        </tbody>
      </table>
    ) : (
      <p>No items</p>
    )
  )
}

export default Table;