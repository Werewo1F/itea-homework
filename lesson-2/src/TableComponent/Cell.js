import React from 'react';

const Cell = ({cells, background, color, type, currency, children}) => {
  type = type.toLowerCase();
  let style = {
    backgroundColor: background,
    color: color,
    textAlign: (type === 'number' || type === 'money') ? 'right' : 'left',
    fontStyle: (type === 'date') ? 'italic' : 'normal'
  }
  if(type === 'money' && currency === '') {
    console.log('No currency option for this parameter');
  }

  return (
    <td
      colSpan={cells}
      style={style}
    >
      {children}
      {
        (type === 'money' && currency && children.indexOf(currency) === -1) ? currency : null
      }
    </td>
  )
}

Cell.defaultProps = {
  type: 'text',
  cells: 1,
  background: 'transparent',
  color: 'black'
}


export default Cell;
