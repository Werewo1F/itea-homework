import React, { Component } from 'react';


import Table from './TableComponent/Table';
import Row from './TableComponent/Row';
import Cell from './TableComponent/Cell';

import LoaderImg from './LoaderImg/'

class App extends Component {
  render() {
    return (
      <div className="wrap">
        <LoaderImg
          src="https://media.springernature.com/lw630/nature-cms/uploads/cms/pages/2913/top_item_image/cuttlefish-e8a66fd9700cda20a859da17e7ec5748.png"
          alt="preload image"
        />
        <Table>
          <Row head={true}>
            <Cell type="" background="red">1</Cell>
            <Cell type="date">2</Cell>
            <Cell type="number">3</Cell>
            <Cell type="money" currency="$">4$</Cell>
          </Row>
          <Row>
            <Cell type="" background="grey">1</Cell>
            <Cell color="green" type="date">2</Cell>
            <Cell cells="2" type="money" currency="$">4</Cell>
          </Row>
        </Table>
      </div>
    );
  }
}

export default App;