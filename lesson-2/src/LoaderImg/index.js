import React, {Component} from 'react';
import './style.css'

class LoaderImg extends Component {

  state = {
    loaded: false
  }

  componentDidMount() {
    let {src} = this.props;
    let img = new Image();
    img.src = src;
    let Loading;
    console.time(Loading);
    img.onload = () => {
      this.setState({loaded: true});
      console.timeEnd(Loading);
    }
  }

  static defaultProps = {
    alt: 'default alt'
  }

  render() {
    const {src, alt} = this.props;
    return (
      <div className="image-wrap">
        {
          this.state.loaded ? (
              <img src={src} alt={alt}/>
            ) :
            (
              <div className="loading"></div>
            )
        }
      </div>
    )
  }
}

export default LoaderImg;