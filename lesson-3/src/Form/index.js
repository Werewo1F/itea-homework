import React from 'react';
import './style.css'

const Form = ({children}) => {
  return (
    <form>
      {children}
    </form>
  )
}

export default Form;