import React, {Component} from 'react';
import PropTypes from "prop-types";

class InputFile extends Component {

  render() {
    const {label, type, name, src, handler} = this.props
    return (
      <div className="form-item">
        <label onChange={handler}>
          <h3>{label}</h3>
          <input
            className="hidden"
            type={type}
            data-name={name}
          />
          <div className="img-wrap">
            <img src={src} alt={label}/>
          </div>
        </label>
      </div>
    )
  }
}

InputFile.defaultProps = {
  src: 'https://mp3bytes.xyz/assets/images/empty.png'
}

InputFile.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['file']).isRequired
}

export default InputFile;