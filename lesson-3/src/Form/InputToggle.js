import React, {Component} from 'react';
import PropTypes from 'prop-types';

export class InputToggle extends Component {

  render() {
    let {children, label, name, handler, activeState} = this.props;
    return (
      <div className="form-item">
        <h3>{label}</h3>
        <div className="flex">
          {
            React.Children.map(
              children,
              (childItem) => {
                let {label} = childItem.props;
                return React.cloneElement(
                  childItem,
                  {
                    label: label,
                    handler: handler,
                    name: name,
                    activeState: activeState
                  }
                )
              }
            )
          }
        </div>
      </div>
    )
  }
}

InputToggle.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['toggle']).isRequired
}

export const ToggleItem = ({label, handler, name, activeState}) => {
  let labelClass = (activeState === label) ? 'item is-active': 'item';
  return (
    <label className={labelClass}>
      {label}
      <input onClick={handler} data-name={name} name={name} value={label} type="radio"/>
    </label>
  )
}