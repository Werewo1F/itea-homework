import React from 'react';
import PropTypes from 'prop-types';

const InputText = ({label, type, placeholder, name, handler}) => {
  return (
    <div className="form-item">
      <label>
        <h3>{label}</h3>
        <input data-name={name} onChange={handler} name={name} placeholder={placeholder} type={type}/>
      </label>
    </div>
  )
}

InputText.propTypes = {
  label: PropTypes.string,
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['text']).isRequired
}

export default InputText;