import React, {Component} from 'react';
import './App.css';

import ButtonRefs from './Refs'
import Form from './Form'
import {InputToggle, ToggleItem} from './Form/InputToggle'
import InputText from './Form/InputText'
import InputFile from './Form/InputFile'

class App extends Component {
  state = {
    toggle: false,
    text: '',
    file: 'https://mp3bytes.xyz/assets/images/empty.png'
  }

  loadImg = (e) => {
    let input = e.target;
    let name = e.target.dataset.name;
    let reader = new FileReader();
    let dataURL;
    reader.onload = () => {
      dataURL = reader.result;
      this.setState({
        [name]: dataURL
      })
    }
    reader.readAsDataURL(input.files[0]);
  }

  handler = (e) => {
    let input = e.target;
    let name = e.target.dataset.name;
    if (input.type === 'file') {
      let reader = new FileReader();
      let dataURL;
      reader.onload = () => {
        dataURL = reader.result;
        this.setState({
          [name]: dataURL
        })
      }
      reader.readAsDataURL(input.files[0]);
    } else {
      this.setState({
        [name]: input.value
      })
    }
  }

  render() {
    const {toggle, file} = this.state;
    const {loadImg, handler} = this;
    return (
      <div className="App">
        <div className="buttons">
          <ButtonRefs
            text="first button"
          />
          <ButtonRefs
            text="second button"
          />
          <ButtonRefs
            text="third button"
          />
          <ButtonRefs
            text="last button"
          />
        </div>
        <Form>
          <InputToggle
            label="Toggler"
            type="toggle"
            name="toggle"
            activeState={toggle}
            handler={handler}
          >
            <ToggleItem
              label="male"
            />
            <ToggleItem
              label="female"
            />
          </InputToggle>
          <InputText
            type="text"
            name="text"
            label="Textfield"
            placeholder="Write text"
            handler={handler}
          />
          <InputFile
            label="Drop your file"
            type="file"
            name="file"
            src={file}
            handler={loadImg}
          />
        </Form>
      </div>
    );
  }
}

export default App;
