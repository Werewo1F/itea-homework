import React, {Component} from 'react';
import './style.css'

class ButtonRef extends Component {
  inputRef = React.createRef();

  handler = (e) => {
    let {animation} = this.props;
    this.inputRef.current.classList.add(animation);
  }

  render() {
    const {handler} = this;
    const {text} = this.props;
    return (
      <button
        ref={this.inputRef}
        onClick={handler}
      >
        <span>{text}</span>
      </button>
    )
  }
}

ButtonRef.defaultProps = {
  animation: "clicked"
}

export default ButtonRef;