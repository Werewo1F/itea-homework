import {combineReducers} from "redux";

import base from './base';

const reducer = combineReducers({
  base
})

export default reducer;