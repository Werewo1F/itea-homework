import {
  FETCH_REQ,
  FETCH_RES,
  FETCH_ERR,
  FETCH_MORE
} from '../actions';

const baseState = {
  loading: false,
  loaded: false,
  data: [],
  error: false
}

const base = (state = baseState, actions) => {
  switch (actions.type) {
    case FETCH_REQ:
      return {
        ...state,
        loading: true
      };
    case FETCH_RES:
      console.log(state);
      console.log(actions.payload);
      return {
        ...state,
        loading: false,
        loaded: true,
        data: actions.payload
      };
    case FETCH_ERR:
      return {
        ...state,
        error: true
      };
    case FETCH_MORE:
      return {
        ...state,
        loading: false,
        data: {
          info: actions.payload.info,
          results: [...state.data.results, ...actions.payload.results]
        }
      };

    default:
      return state;
  }
}

export default base;