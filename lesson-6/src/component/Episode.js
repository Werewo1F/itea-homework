import React, {Component} from 'react';
import moment from 'moment';
import {Link} from "react-router-dom";
import {connect} from "react-redux";

import LoaderImg from "./LoaderImg";

import {GetMainData} from "../actions";

class Episode extends Component {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    const {loading, loaded, data, error} = this.props;
    // let replace = 'https://rickandmortyapi.com/api/';
    return (
      <>
        {
          (loading) ? (
            <div className="loading"></div>
          ) : (
            <>
              {
                (loaded && !error && data.id) ? (
                  <article>
                    <h1>{data.name}</h1>
                    <p>{moment(data.air_date).format("DD/MM/YYYY")}</p>
                    <h4>Characters:</h4>
                    {
                      (data.characters && Array.isArray(data.characters)) && (
                        <ul className="list-img">
                          {
                            data.characters.map(el => (
                              <li key={el.id}>
                                <Link to={`/characters/${el.id}`}>
                                  {
                                    (el.image) && (
                                      <LoaderImg
                                        src={el.image}
                                        alt={el.name}
                                      />
                                    )
                                  }
                                  <h5>{el.name}</h5>
                                </Link>
                              </li>
                            ))
                          }
                        </ul>
                      )
                    }
                  </article>
                ) : (
                  <h1>{data.error}</h1>
                )
              }
            </>
          )
        }
      </>
    )
  }
}


/**
 * Redux
 */
const mapStateToProps = (state, ownProps) => {
  let {loading, loaded, data, error} = state.base;
  return ({
    loading: loading,
    loaded: loaded,
    data: data,
    error: error
  });
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  getData: () => {
    dispatch(GetMainData(`https://rickandmortyapi.com/api/episode/${ownProps.match.params.episodeid}`));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Episode);