import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {connect} from "react-redux";

import LoaderImg from "../LoaderImg";
import {GetMainData} from "../../actions";

class CharacterItem extends Component {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    const {loading, loaded, data, error} = this.props;
    let replace = 'https://rickandmortyapi.com/api/';
    return (
      <>
        {
          (loading) ? (
            <div className="loading"></div>
          ) : (
            <>
              {
                (loaded && !error && data.id) ? (
                  <article>
                    <h1>{data.name}</h1>
                    <LoaderImg
                      src={data.image}
                      alt={data.name}
                    />
                    <p><i>{data.gender}</i></p>
                    <p><b>Status:</b> {data.status}</p>
                    <p><b>Species:</b> {data.species}</p>
                    {
                      (data.origin) && (
                        <>
                          {
                            (data.origin.url.length > 0) ? (
                              <p><b>Origin:</b> <Link to={`/${data.origin.url.replace(replace,'')}`}>{data.origin.name}</Link></p>
                            ) : (
                              <p><b>Origin:</b> {data.origin.name}</p>
                            )
                          }
                        </>
                      )
                    }
                    {
                      (data.location) && (
                        <>
                          {
                            (data.location.url.length > 0) ? (
                              <p><b>Location:</b> <Link to={`/${data.location.url.replace(replace,'')}`}>{data.location.name}</Link></p>
                            ) : (
                              <p><b>Location:</b> {data.location.name}</p>
                            )
                          }
                        </>
                      )
                    }
                    <h4>Episodes:</h4>
                    {
                      (data.episode && Array.isArray(data.episode)) && (
                        <ul className="list">
                          {
                            data.episode.map(el=>{
                              return (
                                <li key={el.id}>
                                  <Link to={`/episode/${el.id}`}>{el.name}</Link>
                                </li>
                              )
                            })
                          }
                        </ul>
                      )
                    }
                  </article>
                ) : (
                  <h1>{data.error}</h1>
                )
              }
            </>
          )
        }
      </>
    )
  }
}


/**
 * Redux
 */
const mapStateToProps = (state, ownProps) => {
  let {loading, loaded, data, error} = state.base;
  return ({
    loading: loading,
    loaded: loaded,
    data: data,
    error: error
  });
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  getData: () => {
    dispatch(GetMainData(`https://rickandmortyapi.com/api/character/${ownProps.match.params.characterid}`));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CharacterItem);