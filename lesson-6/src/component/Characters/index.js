import React from 'react';
import {Route, Switch} from 'react-router-dom';

import CharacterList from './CharactersList';
import CharacterItem from './CharacterItem';

const Characters = () => {
  return (
    <>
      <Switch>
        <Route exact path="/characters" component={CharacterList}/>
        <Route exact path="/characters/:characterid" component={CharacterItem}/>
      </Switch>
    </>
  )
}

export default Characters;