import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from "react-router-dom";

import {GetMainData, UpdMainData} from '../../actions/';
import Button from "../Button";

class CharactersList extends Component {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    const {loading, loaded, data, error, updData} = this.props;
    return (
      <>
        <h1>Characters List</h1>
        {
          (loaded && !error && data.results && data.results.length > 0) ? (
            <>
              {
                data.results.map(el => (
                    <div key={el.id} className='item'>
                      <h2>
                        <Link to={`/characters/${el.id}`}>{el.name}</Link>
                      </h2>
                    </div>
                  )
                )
              }
              {
                (loading) ? (
                  <div className="loading"></div>
                ) : null
              }
              <div className="actions">
                {
                  (data.info.next !== '') ? (
                    <Button
                      label="Show more"
                      handler={updData}
                      url={data.info.next}
                    />
                  ) : null
                }
              </div>
            </>
          ) : (
            (loading) ? (
              <div className="loading"></div>
            ) : (
              <p>error</p>
            )
          )
        }
      </>
    )
  }
}

/**
 * Redux
 */
const mapStateToProps = (state, ownProps) => {
  let {loading, loaded, data, error} = state.base;
  return ({
    loading: loading,
    loaded: loaded,
    data: data,
    error: error
  });
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  getData: () => {
    dispatch(GetMainData('https://rickandmortyapi.com/api/character/'));
  },
  updData: (e) => {
    dispatch(UpdMainData(e.target.dataset.url));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(CharactersList);