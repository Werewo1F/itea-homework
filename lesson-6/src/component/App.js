import React, { Component } from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter, NavLink, Switch, Route} from 'react-router-dom';

import Main from './Main';
import Characters from './Characters/';
import Episode from './Episode';
import Location from './Location';

import '../styles.css';
import store from '../redux/store';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <main>
            <nav>
              <NavLink exact to="/">Home Page</NavLink>
              <NavLink exact to="/characters">Characters</NavLink>
            </nav>
            <Switch>
              <Route exact path="/" component={Main} />
              <Route path="/characters" component={Characters} />
              <Route exact path="/episode/:episodeid" component={Episode} />
              <Route exact path="/location/:locationid" component={Location} />
            </Switch>
          </main>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
