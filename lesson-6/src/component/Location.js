import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {connect} from "react-redux";

import LoaderImg from "./LoaderImg";

import {GetMainData} from "../actions";

class Location extends Component {
  componentDidMount() {
    this.props.getData();
  }

  render() {
    const {loading, loaded, data, error} = this.props;
    return (
      <>
        {
          (loading) ? (
            <div className="loading"></div>
          ) : (
            <>
              {
                (loaded && !error && data.id) ? (
                  <article>
                    <h1>{data.name}</h1>
                    <p>{data.type}</p>
                    <p>{data.dimension}</p>
                    <h4>Residents:</h4>
                    {
                      (data.residents && Array.isArray(data.residents)) && (
                        <ul className="list-img">
                          {
                            data.residents.map(el=>(
                              <li key={el.id}>
                                <Link to={`/characters/${el.id}`}>
                                  {
                                    (el.image) && (
                                      <LoaderImg
                                        src={el.image}
                                        alt={el.name}
                                      />
                                    )
                                  }
                                  <h5>{el.name}</h5>
                                </Link>
                              </li>
                            ))
                          }
                        </ul>
                      )
                    }
                  </article>
                ) : (
                  <h1>{data.error}</h1>
                )
              }
            </>
          )
        }
      </>
    )
  }
}


/**
 * Redux
 */
const mapStateToProps = (state, ownProps) => {
  let {loading, loaded, data, error} = state.base;
  return ({
    loading: loading,
    loaded: loaded,
    data: data,
    error: error
  });
}

const mapDispatchToProps = (dispatch, ownProps) => ({
  getData: () => {
    dispatch(GetMainData(`https://rickandmortyapi.com/api/location/${ownProps.match.params.locationid}`));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(Location);