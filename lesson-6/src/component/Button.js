import React from 'react';
import PropTypes from 'prop-types';


const Button = ({label, handler, url}) => {
  return (
    <button onClick={handler} data-url={url}>
      <span>{label}</span>
    </button>
  )
}

Button.defaultProps = {
  label: 'Start'
}
Button.propTypes = {
  handler: PropTypes.func.isRequired
}

export default Button