import {
  FETCH_RES
} from '../../actions'

const transformRes = store => next => action => {

  const url = 'https://rickandmortyapi.com/api/';



  if (action.type === FETCH_RES && action.payload.id !== undefined) {
    let reqArray, reqUrl, reqResult;
    let {episode, characters, residents} = action.payload;
    if (episode && Array.isArray(episode)) {
      reqResult = 'episode';
      reqArray = episode.map(el=> {
        return el.replace(`${url}${reqResult}/`, '')
      });
      reqUrl = `${url}${reqResult}/${reqArray}`;
    }
    if (characters && Array.isArray(characters)) {
      reqResult = 'character';
      reqArray = characters.map(el=> {
        return el.replace(`${url}${reqResult}/`, '')
      });
      reqUrl = `${url}${reqResult}/${reqArray}`;
      reqResult = 'characters';
    }
    if (residents && Array.isArray(residents)) {
      reqResult = 'character';
      reqArray = residents.map(el=> {
        return el.replace(`${url}${reqResult}/`, '')
      });
      reqUrl = `${url}${reqResult}/${reqArray}`;
      reqResult = 'residents';
    }
    fetch(reqUrl)
      .then( res => res.json() )
      .then( res => {
        res = (res.id) ? [res] : res;
        action.payload[reqResult] = res;
        return next(action);
      })

  } else {
    return next(action);
  }


}

export default transformRes;