export const FETCH_REQ = 'FETCH_REQ';
export const FETCH_RES = 'FETCH_RES';
export const FETCH_MORE = 'FETCH_MORE';
export const FETCH_ERR = 'FETCH_ERR';
export const FETCH_CHILD = 'FETCH_CHILD';


const fetchResponse = ( data ) => ({
  type: FETCH_RES,
  payload: data
});
const fetchMore = ( data ) => ({
  type: FETCH_MORE,
  payload: data
});


export const GetMainData = ( src ) => ( dispatch, getState ) => {
  dispatch({type: FETCH_REQ});
  fetch(src)
    .then( res => res.json() )
    .then( res => dispatch( fetchResponse(res) ))
    .catch( error => dispatch({ type: FETCH_ERR, error }));
}


export const UpdMainData = ( src ) => ( dispatch, getState ) => {
  dispatch({type: FETCH_REQ});
  fetch(src)
    .then( res => res.json() )
    .then( res => dispatch( fetchMore(res) ))
    .catch( error => dispatch({ type: FETCH_ERR, error }));
}
