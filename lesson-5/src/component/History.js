import React, {Component} from 'react';
import {connect} from 'react-redux';

import Img from './Img';

class History extends Component {

  render() {
    const {searchHistory, loadFromHistory} = this.props;
    return (
      <div className="item item__history">
        <h2>History</h2>
        {
          (searchHistory.length > 0) ? (
            searchHistory.map((el, index) => (
              <p key={index}>
                <span  onClick={loadFromHistory(el.name)}>{el.name}, {parseInt(el.main.temp)}°</span>
                <Img img={el.weather[0].icon}/>
              </p>
            ))
          ) : (
            <p>history is empty</p>
          )
        }
      </div>
    )
  }
}

/*
* Redux
 */

const stateToProps = (state, ownProps) => ({
  searchHistory: state.searchHistory
});

const dispatchToProps = (dispatch, ownProps) => ({
  loadFromHistory: ( el ) => (e) => {
    dispatch({type: 'LOAD_START'});
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${el}&units=metric&appid=f581b3167ebb40af8cd9e70d9c43dfde&lang=RU`)
      .then(response => response.json())
      .then(json => {
        dispatch({type: 'LOAD_FINISH', payload: json});
        if(json.cod === 200) {
          dispatch({type: 'UPDATE_HISTORY', payload: json});
        }
      })
  }
});

export default connect(stateToProps,dispatchToProps)(History);