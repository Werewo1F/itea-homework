import React from 'react';

import Images from '../weatherIcon';

const Img = ({img}) => {
  return (
    <img src={Images[`id_${img}`]} alt="weather"/>
  )
}

export default Img;