import React from 'react';

const Wind = ({wind}) => {
  /*
  * Logic for speed and direction
   */
  return (
    <>{wind.speed}m/s</>
  )
}

export default Wind;