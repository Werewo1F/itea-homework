import React, {Component} from 'react';
import {connect} from 'react-redux';

class Input extends Component {

  changeHandler = (e) => {
    let {value} = e.target;
    clearTimeout (this.interval);
    this.interval = setTimeout(() => {
      this.props.loaded(value);
    },500)
  }

  componentDidMount() {
    this.props.loaded('Kiev');
  }

  render() {
    const {changeHandler} = this;
    return (
      <input onChange={changeHandler} placeholder="Введите город для поиска" type="text"/>
    )
  }
}

const dispatchToProps = (dispatch, ownProps) => ({
  loaded: (e) => {
    dispatch({type: 'LOAD_START'});
    fetch(`https://api.openweathermap.org/data/2.5/weather?q=${e}&units=metric&appid=f581b3167ebb40af8cd9e70d9c43dfde&lang=RU`)
      .then(response => response.json())
      .then(json => {
        dispatch({type: 'LOAD_FINISH', payload: json});
        if(json.cod === 200) {
          dispatch({type: 'UPDATE_HISTORY', payload: json});
        }
      })
  }
});

export default connect(false, dispatchToProps)(Input);