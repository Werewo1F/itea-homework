import React, {Component} from 'react';
import {connect} from 'react-redux';

import Wind from './Wind';
import Img from './Img';

class Result extends Component {

  render() {
    const {searchLoaded, searchResult} = this.props;
    return (
      <div className="item item__result">
        <h2>Search result:</h2>
        <div className="inner">
          {
            (searchLoaded) ? (
              (searchResult.cod === 200) ? (
                <>
                  <div className="img-wrap">
                    <Img img={searchResult.weather[0].icon}/>
                  </div>
                  <h3>{searchResult.name}</h3>
                  <p>{parseInt(searchResult.main.temp)}°C</p>
                  <p>
                    Wind: <Wind wind={searchResult.wind} /> <br/>
                    Visibility: {searchResult.weather[0].main}
                  </p>
                </>
              ) : (
                <h2>{searchResult.message}</h2>
              )
            ) : (
              <div className="loading"></div>
            )
          }
        </div>
      </div>
    )
  }
}

/*
* Redux
 */

const stateToProps = (state, ownProps) => ({
  searchLoaded: state.searchLoaded,
  searchResult: state.searchResult
});

export default connect(stateToProps, false)(Result);