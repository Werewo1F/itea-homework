const initialState = {
  searchLoaded: false,
  searchResult: {},
  searchHistory: []
}


function reducer(state = initialState, action) {
  switch (action.type) {
    case 'LOAD_START': {
      return {
        ...state,
        searchLoaded: false
      }
    }
    case 'LOAD_FINISH': {
      return {
        ...state,
        searchLoaded: true,
        searchResult: action.payload
      }
    }
    case 'UPDATE_HISTORY':
      return {
        ...state,
        searchHistory: [
          action.payload,
          ...state.searchHistory
        ]
      }

    default:
      return state;
  }
}

export default reducer;