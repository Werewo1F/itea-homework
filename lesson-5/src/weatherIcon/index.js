import id_01d from './sun.svg';
import id_01n from './moon.svg';
import id_02d from './cloudd.svg';
import id_02n from './cloudn.svg';
import id_03d from './cloud.svg';
import id_03n from './cloud.svg';
import id_04d from './cloudes.svg';
import id_04n from './cloudes.svg';
import id_09d from './shower-rain.svg';
import id_09n from './shower-rain.svg';
import id_10d from './rain.svg';
import id_10n from './rain.svg';
import id_11d from './storm.svg';
import id_11n from './storm.svg';
import id_13d from './snow.svg';
import id_13n from './snow.svg';
import id_50d from './fog.svg';
import id_50n from './fog.svg';


export default {id_01d, id_01n, id_02d, id_02n, id_03d, id_03n, id_04d, id_04n, id_09d, id_09n, id_10d, id_10n, id_11d, id_11n, id_13d, id_13n, id_50d, id_50n};