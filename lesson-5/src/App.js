import React, {Component} from 'react';
import {Provider} from 'react-redux';

import store from './redux/store';
import Result from './component/Result'
import Input from './component/Input'
import History from './component/History'

import './App.css';

class App extends Component {

  render() {
    const {onChange} = this;
    return (
      <Provider store={store}>
        <div className="App">
          <h1>Super weather App</h1>
          <Input
            handler={onChange}
          />
          <div className="wrap">
            <Result/>
            <History/>
          </div>
        </div>
      </Provider>
    );
  }
}

export default App;