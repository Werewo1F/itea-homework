import React, {Component} from 'react';
import {BrowserRouter, Switch, Route, NavLink} from 'react-router-dom';

import Routes from './routes';
import Users from './pages/Users'


import './App.css';

class App extends Component {
  state = {
    user: 0
  }

  changeUser = (el) => {
    this.setState({
      user: el.target.dataset.id
    })
  }

  render() {
    const {user} = this.state;
    const {changeUser} = this;
    return (
      <BrowserRouter>
        <div className="App">
          <header>
            <div className="logo">MyLogo</div>
            <nav>
              <NavLink exact activeClassName="active" to="/">Main page</NavLink>
              <NavLink activeClassName="active" to="/posts/limit/3">Last news</NavLink>
              <NavLink activeClassName="active" to="/posts/1">Post</NavLink>
              <NavLink activeClassName="active" to="/users">Users</NavLink>
            </nav>
          </header>
          <main>
            <Switch>
              <Route
                key={Routes.length + 1}
                exact
                path="/users"
                render={
                  () => {
                    return <Users
                      user={user}
                      changeUser={changeUser}
                    />
                  }
                }
              />
              {
                Routes.map((el, index) => {
                  return <Route
                    key={index}
                    {...el}
                  />
                })
              }
            </Switch>
          </main>
        </div>
      </BrowserRouter>
    );
  }
}

export default App;