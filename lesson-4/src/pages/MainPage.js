import React, {Component} from 'react';
import {Link} from 'react-router-dom';

import Button from './../components/Button'

class MainPage extends Component {
  state = {
    show: 0,
    willShowed: 20,
    posts: false
  }

  showMore = () => {
    const {show, posts, willShowed} = this.state;
    let nextShow = (show + willShowed >= posts.length) ? 0 : show + willShowed;
    this.setState({
      show: nextShow
    });
  }

  componentDidMount() {
    let newShowed = (this.props.match.params.postlimit) ? this.props.match.params.postlimit * 1 : 20;
    this.setState({
      willShowed: newShowed
    });
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(json => {
        this.setState({
          posts: json
        })
      })
  }

  render() {

    let {posts, show, willShowed} = this.state;
    let {showMore} = this;
    return (
      <>
        {
          (posts) ?
            (
              <>
                <ul className="list">
                  {
                    posts.map((el, index)=>
                      (index >= show && index < show + willShowed) ? (
                        <li key={index} className="item">
                          <Link to={`/posts/${el.id}`}>{el.title}</Link>
                        </li>
                        ) : null
                    )
                  }
                </ul>
                <Button
                  label="Show more"
                  handler={showMore}
                />
              </>
            ) :
            (
              <div className="loading"></div>
            )
        }
      </>
    )
  }
}

export default MainPage;