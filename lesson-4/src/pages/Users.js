import React, {Component} from 'react';

import Button from './../components/Button';

class Users extends Component {
  state = {
    users: false
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response => response.json())
      .then(json => {
        this.setState({
          users: json
        })
      })
  }

  render() {
    let {users} = this.state;
    const {user, changeUser} = this.props;
    return (
      (users) ? (
        users.map(el => {
          return (
            <div className="item item--user">
              {
                (user === 0) ? (
                  <Button
                    label="Change user"
                    handler={changeUser}
                    dataId={el.id}
                  />
                ) : null
              }
              <h3>{el.name}</h3>
              <a href={`mailto:${el.email}`}>{el.email}</a>
            </div>
          )
        })
      ) : (
        <div className="loading"></div>
      )
    )
  }
}

export default Users;