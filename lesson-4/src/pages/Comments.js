import React, {Component} from 'react';

class Comments extends Component {
  state = {
    comments: false
  }

  componentDidMount() {
    let {postid} = this.props;
    fetch(`https://jsonplaceholder.typicode.com/posts/${postid}/comments`)
      .then(response => response.json())
      .then(json => {
        this.setState({
          comments: json
        })
      })
  }

  render() {
    const {comments} = this.state;
    return (
      (comments) ? (
        comments.map(el=>
          <div className="comment">
            <h3>{el.name}</h3>
            <a href={`mailto:${el.email}`}>{el.email}</a>
            <p>{el.body}</p>
          </div>
        )
      ) : (
        <div className="loading"></div>
      )
    )
  }
}

export default Comments;