import React, {Component} from 'react';

import Button from './../components/Button';
import Comments from './Comments';

class Post extends Component {
  state = {
    comments: false,
    post: false
  }

  showComments = () => {
    this.setState({
      comments: true
    })
  }

  componentDidMount() {
    let {postid} = this.props.match.params;
    fetch(`https://jsonplaceholder.typicode.com/posts/${postid}`)
      .then(response => response.json())
      .then(json => {
        this.setState({
          post: json
        })
      })
  }

  render() {
    const {showComments} = this;
    const {post, comments} = this.state;
    let {postid} = this.props.match.params;
    return (
      <>
        {
          (post) ?
            (
              <>
                <h2>{post.title}</h2>
                <article>{post.body}</article>
                {
                  (comments) ? (
                    <Comments
                      postid={postid}
                    />
                  ) : (
                    <Button
                      label="Show comments"
                      handler={showComments}
                    />
                  )
                }
              </>
            ) :
            (
              <div className="loading"></div>
            )
        }
      </>
    )
  }
}

export default Post;