import React, {Component} from 'react';
import {Link} from 'react-router-dom';

class MainPage extends Component {
  state = {
    show: 0,
    willShowed: 20,
    posts: false
  }

  componentDidMount() {
    let newShowed = (this.props.match.params.postlimit) ? this.props.match.params.postlimit * 1 : 20;
    this.setState({
      willShowed: newShowed
    });
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then(response => response.json())
      .then(json => {
        this.setState({
          posts: json
        })
      })
  }

  render() {

    let {posts, show, willShowed} = this.state;
    let outputList = false;

    if(posts) {
      let currentQuery = posts.filter((el, index) => (index >= show && index < show + willShowed));
      outputList = currentQuery.map(el=>
        <li className="item">
          <Link to={`/posts/${el.id}`}>{el.title}</Link>
        </li>
      );
    }
    return (
      <>
        {
          (outputList) ?
            (
              <>
                <ul className="list">
                  {outputList}
                </ul>
              </>
            ) :
            (
              <div className="loading"></div>
            )
        }
      </>
    )
  }
}

export default MainPage;