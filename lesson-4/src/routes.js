import MainPage from './pages/MainPage'
import Post from './pages/Post'
import Limit from './pages/Limit'
import PageNotFound from './pages/pageNotFound'


const Routes = [
  {
    path: "/",
    component: MainPage,
    exact: true
  },
  {
    exact: true,
    path: "/posts/:postid",
    component: Post
  },
  {
    path: "/posts/limit/:postlimit",
    component: Limit
  },
  {
    component: PageNotFound
  }
]

export default Routes;